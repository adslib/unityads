# Unity Ads #

此專案測試/展示 **Unity 2019.4.10.f1** 使用 **Unity Ads(Advertisement 3.4.9)** 的使用流程

### UnityAds相關服務啟用流程 ###

1. [創建 Unity 帳號](https://unity.com/solutions/unity-ads)
2. 開啟Unity的 [Dashboard](https://dashboard.unity3d.com/landing) 並選擇 "Monetize" 服務
3. 新增廣告單元(Placements)

### UnityAds套件安裝方法 ###

* 從 Asset store [下載](https://assetstore.unity.com/packages/add-ons/services/unity-ads-66123?_ga=2.5721392.284662792.1603677360-1214003009.1600672966),
* 使用 Package Manager 安裝, 依照下列步驟(推薦):
  1.開啟Unity, 上方工具列 > Window > Package Manager 開啟 Package Manager.
  2.搜尋關鍵字 "Advertisement", 並選擇已驗證的(verified)版本
  3.點擊 Install 或 Update 按鈕

> **警告**: 上方兩個選項選其一執行, 如果兩個都做會導致build error

### 注意事項及常見問題 ###

1. 在callback事件中, OnUnityAdsDidStart這個callback觸發的時機是在顯示完廣告回到app時才會觸發, 跟Admob略有不同