﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class Demo : MonoBehaviour, IUnityAdsListener
{
    public Text adStatusText;

#if UNITY_ANDROID
    string gameId = "3877771";
#elif UNITY_IOS
    string gameId = "3877770";
#endif

    string bannerPlacementId = "Banner";
    string interstitialPlacementId = "video";
    string rewardedVideoPlacementId = "rewardedVideo";

    bool testMode = true;
    bool isBannerShowed = false;

    void Start()
    {
        Advertisement.AddListener (this); // 如果有繼承IUnityAdsListener並實作函式, 須在呼叫這行才能正確使用callback
        Advertisement.Initialize(gameId, testMode); // 初始化
        Advertisement.Banner.SetPosition (BannerPosition.BOTTOM_CENTER); // 設定banner的位置(螢幕相對位置)
    }

    public void ShowBanner()
    {
        if (Advertisement.isInitialized)
        {
            if (isBannerShowed == false)
            {
                Advertisement.Banner.Show(bannerPlacementId);
                isBannerShowed = true;
            }
            else
            {
                Advertisement.Banner.Hide();
                isBannerShowed = false;
            }
        }
        else
        {
            UpdateAdStatus("AD is not initialized");
        }
    }

    public void ShowInterstitialAd()
    {
        if (Advertisement.IsReady())
        {
            Advertisement.Show(interstitialPlacementId);
        }
        else
        {
            UpdateAdStatus("Interstitial ad not ready at the moment! Please try again later!");
        }
    }

    public void ShowRewardedVideo()
    {
        if (Advertisement.IsReady(rewardedVideoPlacementId))
        {
            Advertisement.Show(rewardedVideoPlacementId);
        }
        else
        {
            UpdateAdStatus("Rewarded video is not ready at the moment! Please try again later!");
        }
    }

    // IUnityAdsListener interface
    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        UpdateAdStatus("Placement id: " + placementId + ", result: " + showResult.ToString());

        if (showResult == ShowResult.Finished)
        {
            
        }
        else if (showResult == ShowResult.Skipped)
        {
            
        }
        else if (showResult == ShowResult.Failed)
        {
            
        }
    }

    public void OnUnityAdsReady(string placementId)
    {
        UpdateAdStatus(placementId + " is ready");
    }

    public void OnUnityAdsDidError(string message)
    {
        UpdateAdStatus("UnityAds did error, msg: " + message);
    }

    // 這邊的呼叫雖然是DidStart, 不過廣告開始時會跳離app, 在廣告結束時再返回, 所以沒法在一開始時執行
    public void OnUnityAdsDidStart(string placementId)
    {
        UpdateAdStatus(placementId + " is starting");
    }

    // callback清除
    public void OnDestroy()
    {
        Advertisement.RemoveListener(this);
    }

    void UpdateAdStatus(string message)
    {
        string prefix = "======== ";
        Debug.Log(prefix + message);
        adStatusText.text = "AD status: " + message;
    }
}
